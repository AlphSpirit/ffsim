export default {
	"BRD": {
		id: 5,
		name: "Bard",
		role: "ranged"
	},
	"AST": {
		id: 33,
		name: "Astrologian",
		role: "healer"
	}
};