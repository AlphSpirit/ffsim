import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    className: "brd",
    search: {
      name: "",
      server: "Exodus"
    },
    stats: {
      strength: 305,
      dexterity: 3847,
      intelligence: 291,
      criticalHit: 2568,
      determination: 2023,
      directHit: 2186,
      skillSpeed: 870,
      spellSpeed: 0,
      weaponDamage: 117,
      weaponDelay: 3.04
    }
  },
  mutations: {

  },
  actions: {

  }
})
