import Spell from "../Spell";
import FlourishingCascade from "../../buffs/dnc/FlourishingCascade";

export default class Cascade extends Spell {

	constructor() {
		super();
		this.id = "cascade";
		this.name = "Cascade";
		this.cast = 0;
		this.range = 25;
		this.recast = 0;
		this.potency = 200;
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		if (Math.random() <= 0.5) {
			engine.addBuff(new FlourishingCascade());
		}
	}

}