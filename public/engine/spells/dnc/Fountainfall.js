import Spell from "../Spell";
import Engine from "../../Engine";

export default class Fountainfall extends Spell {

	constructor() {
		super();
		this.id = "fountainfall";
		this.name = "Fountainfall";
		this.cast = 0;
		this.range = 25;
		this.recast = 0;
		this.potency = 350;
		this.breaksCombos = false;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !!engine.hasBuff("flourishing_fountain");
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.removeBuff(engine.hasBuff("flourishing_fountain"));
		if (Math.random() <= 0.5) {
			engine.resources.get("fourfold_feathers").add(1);
		}
	}

}