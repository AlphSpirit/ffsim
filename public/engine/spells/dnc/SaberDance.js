import Spell from "../Spell";
import Engine from "../../Engine";

export default class SaberDance extends Spell {

	constructor() {
		super();
		this.id = "saber_dance";
		this.name = "Saber Dance";
		this.potency = 600;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("esprit").amount >= 50;
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("esprit").add(-50);
	}

}