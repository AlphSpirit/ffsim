import Spell from "../Spell";
import Engine from "../../Engine";
import FlourishingFanDance from "../../buffs/dnc/FlourishingFanDance";

export default class FanDance extends Spell {

	constructor() {
		super();
		this.id = "fan_dance";
		this.name = "Fan Dance";
		this.cast = 0;
		this.range = 25;
		this.recast = 1000;
		this.potency = 150;
		this.offGcd = true;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("fourfold_feathers").amount > 0;
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("fourfold_feathers").add(-1);
		if (Math.random() <= 0.5) {
			engine.addBuff(new FlourishingFanDance());
		}
	}

}