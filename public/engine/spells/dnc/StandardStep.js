import StandardStepBuff from "../../buffs/dnc/StandardStepBuff";
import Spell from "../Spell";
import Engine from "../../Engine";

export default class StandardStep extends Spell {

	constructor() {
		super();
		this.id = "standard_step";
		this.name = "Standard Step";
		this.cast = 0;
		this.recast = 30 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !engine.hasBuff("technical_step") && !engine.hasBuff("standard_step");
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new StandardStepBuff());
	}

}