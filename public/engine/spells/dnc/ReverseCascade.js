import Spell from "../Spell";
import Engine from "../../Engine";

export default class ReverseCascade extends Spell {

	constructor() {
		super();
		this.id = "reverse_cascade";
		this.name = "Reverse Cascade";
		this.cast = 0;
		this.range = 25;
		this.recast = 0;
		this.potency = 300;
		this.breaksCombos = false;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !!engine.hasBuff("flourishing_cascade");
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.removeBuff(engine.hasBuff("flourishing_cascade"));
		if (Math.random() <= 0.5) {
			engine.resources.get("fourfold_feathers").add(1);
		}
	}

}