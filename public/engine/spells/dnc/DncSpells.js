import Cascade from "./Cascade";
import Fountain from "./Fountain";
import ReverseCascade from "./ReverseCascade";
import Fountainfall from "./Fountainfall";
import FanDance from "./FanDance";
import FanDanceIII from "./FanDanceIII";
import Flourish from "./Flourish";
import Bloodshower from "./Bloodshower";
import RisingWindmill from "./RisingWindmill";
import TechnicalStep from "./TechnicalStep";
import TechnicalFinish from "./TechnicalFinish";
import Emboite from "./Emboite";
import StandardStep from "./StandardStep";
import StandardFinish from "./StandardFinish";
import Devilment from "./Devilment";
import SaberDance from "./SaberDance";

export default [
	new Cascade(),
	new Fountain(),
	new ReverseCascade(),
	new Fountainfall(),
	new FanDance(),
	new FanDanceIII(),
	new Flourish(),
	new Bloodshower(),
	new RisingWindmill(),
	new TechnicalStep(),
	new TechnicalFinish(),
	new Emboite(),
	new StandardStep(),
	new StandardFinish(),
	new Devilment(),
	new SaberDance()
];