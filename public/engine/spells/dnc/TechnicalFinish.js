import Spell from "../Spell";
import Engine from "../../Engine";
import StandardFinishBuff from "../../buffs/dnc/StandardFinishBuff";

export default class TechnicalFinish extends Spell {

	constructor() {
		super();
		this.id = "technical_finish";
		this.name = "Technical Finish";
		this.potency = 1500;
		this.cast = 0;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !!engine.hasBuff("technical_step") && engine.resources.get("steps").amount == 4;
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.removeBuff(engine.hasBuff("technical_step"));
		engine.resources.get("steps").add(-4);
		engine.addBuff(new StandardFinishBuff(20 * 1000));
	}

}