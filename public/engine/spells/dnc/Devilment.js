import Spell from "../Spell";
import DevilmentBuff from "../../buffs/dnc/DevilmentBuff";

export default class Devilment extends Spell {

	constructor() {
		super();
		this.id = "devilment";
		this.name = "Devilment";
		this.recast = 120 * 1000;
		this.offGcd = true;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !engine.hasBuff("technical_step") && !engine.hasBuff("standard_step");
	}

	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new DevilmentBuff());
	}

}