import Spell from "../Spell";
import Engine from "../../Engine";

export default class Emboite extends Spell {

	constructor() {
		super();
		this.id = "emboite";
		this.name = "Emboite";
		this.recast = 1000;
		this.gcd = 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !!engine.hasBuff("technical_step") || !!engine.hasBuff("standard_step");
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("steps").add(1);
	}

}