import Spell from "../Spell";
import TechnicalStepBuff from "../../buffs/dnc/TechnicalStepBuff";

export default class TechnicalStep extends Spell {

	constructor() {
		super();
		this.id = "technical_step";
		this.name = "Technical Step";
		this.cast = 0;
		this.recast = 120 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !engine.hasBuff("technical_step") && !engine.hasBuff("standard_step");
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new TechnicalStepBuff());
	}

}