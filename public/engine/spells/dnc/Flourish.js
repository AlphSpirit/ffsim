import Spell from "../Spell";
import Engine from "../../Engine";
import FlourishingCascade from "../../buffs/dnc/FlourishingCascade";
import FlourishingFountain from "../../buffs/dnc/FlourishingFountain";
import FlourishingFanDance from "../../buffs/dnc/FlourishingFanDance";
import FlourishingShower from "../../buffs/dnc/FlourishingShower";
import FlourishingWindmill from "../../buffs/dnc/FlourishingWindmill";

export default class Flourish extends Spell {

	constructor() {
		super();
		this.id = "flourish";
		this.name = "Flourish";
		this.cast = 0;
		this.offGcd = true;
		this.recast = 60 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !engine.hasBuff("technical_step") && !engine.hasBuff("standard_step");
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new FlourishingCascade());
		engine.addBuff(new FlourishingFountain());
		engine.addBuff(new FlourishingFanDance());
		engine.addBuff(new FlourishingShower());
		engine.addBuff(new FlourishingWindmill());
	}

}