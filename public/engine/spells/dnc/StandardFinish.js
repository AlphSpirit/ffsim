import Spell from "../Spell";
import Engine from "../../Engine";
import StandardFinishBuff from "../../buffs/dnc/StandardFinishBuff";

export default class StandardFinish extends Spell {

	constructor() {
		super();
		this.id = "standard_finish";
		this.name = "Standard Finish";
		this.potency = 1000;
		this.cast = 0;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !!engine.hasBuff("standard_step") && engine.resources.get("steps").amount == 2;
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.removeBuff(engine.hasBuff("standard_step"));
		engine.resources.get("steps").add(-2);
		engine.addBuff(new StandardFinishBuff(60 * 1000));
	}

}