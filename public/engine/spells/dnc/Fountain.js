import FlourishingFountain from "../../buffs/dnc/FlourishingFountain";
import Engine from "../../Engine";
import Spell from "../Spell";

export default class Fountain extends Spell {

	constructor() {
		super();
		this.id = "fountain";
		this.name = "Fountain";
		this.cast = 0;
		this.range = 25;
		this.recast = 0;
		this.potency = 250;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.lastComboAction == "cascade";
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		if (Math.random() <= 0.5) {
			engine.addBuff(new FlourishingFountain());
		}
	}

}