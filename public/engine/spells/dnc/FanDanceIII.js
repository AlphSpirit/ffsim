import Spell from "../Spell";
import Engine from "../../Engine";

export default class FanDanceIII extends Spell {

	constructor() {
		super();
		this.id = "fan_dance_iii";
		this.name = "Fan Dance III";
		this.cast = 0;
		this.range = 25;
		this.recast = 1000;
		this.potency = 200;
		this.offGcd = true;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !!engine.hasBuff("flourishing_fan_dance") && !engine.hasBuff("technical_step") && !engine.hasBuff("standard_step");
	}

	/**
	 * @param {Engine} engine
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.removeBuff(engine.hasBuff("flourishing_fan_dance"));
	}

}