export default class Spell {

	constructor() {
		this.id = "";
		this.name = "";
		this.cast = 0;
		this.offGcd = false;
		this.range = 0;
		this.recast = 0;
		this.recastGcd = false;
		this.potency = 0;
		this.cooldown = 0;
		this.breaksCombos = true;
		this.prepull = false;
		this.gcd = 0;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return true;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		this.cooldown = this.getRecast(engine);
	}

	/**
	 * @param {Engine} engine
	 */
	getPotency(engine) {
		return this.potency;
	}

	/**
	 * @param {Engine} engine
	 */
	canClip(engine) {
		return false;
	}

	/**
	 * @param {Engine} engine
	 */
	getRecast(engine) {
		if (!this.recastGcd) {
			return this.recast;
		}
		return Math.floor(this.recast * engine.stats.getBaseGcdMultiplier(engine) * 100) / 100;
	}

}