import Spell from "../Spell";
import Engine from "../../Engine";

export default class PitchPerfect extends Spell {

	constructor() {
		super();
		this.id = "pitch_perfect";
		this.name = "Pitch Perfect";
		this.cast = 0;
		this.offGcd = true;
		this.range = 25;
		this.recast = 3 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !!engine.hasBuff("wanderers_minuet");
	}

	/**
	 * @param {Engine} engine
	 */
	getPotency(engine) {
		let stacks = engine.resources.get("repertoire_minuet").amount;
		if (stacks == 3) {
			return 450;
		} else if (stacks == 2) {
			return 250;
		} else {
			return 100;
		}
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		engine.resources.get("repertoire_minuet").amount = 0;
	}

}