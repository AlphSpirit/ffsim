import StraightShotReady from "../../buffs/brd/StraightShotReady";
import Engine from "../../Engine";
import Spell from "../Spell";

export default class BurstShot extends Spell {

	constructor() {
		super();
		this.id = "burst_shot";
		this.name = "Burst Shot";
		this.cast = 0;
		this.range = 25;
		this.recast = 0;
		this.potency = 230;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		if (Math.random() <= 0.35) {
			engine.addBuff(new StraightShotReady());
		}
	}

}