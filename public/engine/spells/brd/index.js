import BurstShot from "./BurstShot";
import Stormbite from "./Stormbite";
import CausticBite from "./CausticBite";
import IronJaws from "./IronJaws";
import Bloodletter from "./Bloodletter";
import { RagingStrikes } from "./RagingStrikes";
import MagesBallad from "./MagesBallad";
import Barrage from "./Barrage";
import EmpyrealArrow from "./EmpyrealArrow";
import Sidewinder from "./Sidewinder";
import RefulgentArrow from "./RefulgentArrow";
import WanderersMinuet from "./WanderersMinuet";
import PitchPerfect from "./PitchPerfect";
import ArmysPaeon from "./ArmysPaeon";
import ApexArrow from "./ApexArrow";
import { BattleVoice } from "./BattleVoice";

export default [
	new BurstShot(),
	new Stormbite(),
	new CausticBite(),
	new IronJaws(),
	new Bloodletter(),
	new RagingStrikes(),
	new MagesBallad(),
	new Barrage(),
	new EmpyrealArrow(),
	new Sidewinder(),
	new RefulgentArrow(),
	new WanderersMinuet(),
	new PitchPerfect(),
	new ArmysPaeon(),
	new ApexArrow(),
	new BattleVoice()
];