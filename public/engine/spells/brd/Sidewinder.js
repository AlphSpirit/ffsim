import Spell from "../Spell";
import Engine from "../../Engine";

export default class Sidewinder extends Spell {

	constructor() {
		super();
		this.id = "sidewinder";
		this.name = "Sidewinder";
		this.cast = 0;
		this.offGcd = true;
		this.range = 25;
		this.recast = 60 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	getPotency(engine) {
		let stormbite = engine.hasDebuff("stormbite");
		let causticBite = engine.hasDebuff("caustic_bite");
		if (stormbite && causticBite) {
			return 300;
		}
		if (stormbite || causticBite) {
			return 200;
		}
		return 100;
	}

}