import Spell from "../Spell";
import Engine from "../../Engine";
import MagesBalladBuff from "../../buffs/brd/MagesBalldBuff";

export default class MagesBallad extends Spell {

	constructor() {
		super();
		this.id = "mages_ballad";
		this.name = "Mage's Ballad";
		this.cast = 0;
		this.offGcd = true;
		this.recast = 80 * 1000;
		this.potency = 100;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		let wanderersMinuet = engine.hasBuff("wanderers_minuet");
		if (wanderersMinuet) {
			engine.removeBuff(wanderersMinuet);
		}
		let armysPaeon = engine.hasBuff("armys_paeon");
		if (armysPaeon) {
			engine.removeBuff(armysPaeon);
		}
		engine.addBuff(new MagesBalladBuff());
	}

}