import Spell from "../Spell";
import Engine from "../../Engine";

export default class EmpyrealArrow extends Spell {

	constructor() {
		super();
		this.id = "empyreal_arrow";
		this.name = "Empyreal Arrow";
		this.cast = 0;
		this.offGcd = true;
		this.range = 25;
		this.recast = 15 * 1000;
		this.recastGcd = true;
		this.potency = 230;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		if (engine.hasBuff("mages_ballad")) {
			engine.spells.get("bloodletter").cooldown = 0;
		} else if (engine.hasBuff("wanderers_minuet")) {
			engine.resources.get("repertoire_minuet").add(1);
		} else if (engine.hasBuff("armys_paeon")) {
			engine.resources.get("repertoire_paeon").add(1);
		}
	}

}