import { RagingStrikesBuff } from "../../buffs/brd/RagingStrikesBuff";
import Engine from "../../Engine";
import Spell from "../Spell";

export class RagingStrikes extends Spell {

	constructor() {
		super();
		this.id = "raging_strikes";
		this.name = "Raging Strikes";
		this.cast = 0;
		this.offGcd = true;
		this.range = 25;
		this.recast = 80 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new RagingStrikesBuff());
	}

}