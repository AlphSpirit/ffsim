import StraightShotReady from "../../buffs/brd/StraightShotReady";
import CausticBiteDebuff from "../../debuffs/brd/CausticBiteDebuff";
import Engine from "../../Engine";
import Spell from "../Spell";

export default class CausticBite extends Spell {

	constructor() {
		super();
		this.id = "caustic_bite";
		this.name = "Caustic Bite";
		this.cast = 0;
		this.range = 25;
		this.recastGcd = true;
		this.potency = 150;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addDebuff(new CausticBiteDebuff());
		if (Math.random() <= 0.35) {
			engine.addBuff(new StraightShotReady());
		}
	}

}