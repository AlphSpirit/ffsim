import BarrageBuff from "../../buffs/brd/BarrageBuff";
import StraightShotReady from "../../buffs/brd/StraightShotReady";
import Spell from "../Spell";

export default class Barrage extends Spell {

	constructor() {
		super();
		this.id = "barrage";
		this.name = "Barrage";
		this.cast = 0;
		this.offGcd = true;
		this.range = 25;
		this.recast = 80 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new BarrageBuff());
		engine.addBuff(new StraightShotReady());
	}

}