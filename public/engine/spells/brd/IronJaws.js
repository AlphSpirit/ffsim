import StraightShotReady from "../../buffs/brd/StraightShotReady";
import Engine from "../../Engine";
import Spell from "../Spell";

export default class IronJaws extends Spell {

	constructor() {
		super();
		this.id = "iron_jaws";
		this.name = "Iron Jaws";
		this.cast = 0;
		this.range = 25;
		this.recast = 0;
		this.potency = 100;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		let stormbite = engine.hasDebuff("stormbite");
		if (stormbite) {
			stormbite.refresh(engine.buffs);
		}
		let causticBite = engine.hasDebuff("caustic_bite");
		if (causticBite) {
			causticBite.refresh(engine.buffs);
		}
		if (Math.random() <= 0.35) {
			engine.addBuff(new StraightShotReady());
		}
	}

}