import Spell from "../Spell";
import ArmysPaeonBuff from "../../buffs/brd/ArmysPaeonBuff";

export default class ArmysPaeon extends Spell {

	constructor() {
		super();
		this.id = "armys_paeon";
		this.name = "Army's Paeon";
		this.cast = 0;
		this.offGcd = true;
		this.recast = 80 * 1000;
		this.potency = 100;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		let magesBallad = engine.hasBuff("mages_ballad");
		if (magesBallad) {
			engine.removeBuff(magesBallad);
		}
		let wanderersMinuet = engine.hasBuff("wanderers_minuet");
		if (wanderersMinuet) {
			engine.removeBuff(wanderersMinuet);
		}
		engine.addBuff(new ArmysPaeonBuff());
	}

}