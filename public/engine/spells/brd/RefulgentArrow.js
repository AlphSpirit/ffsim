import Spell from "../Spell";
import Engine from "../../Engine";

export default class RefulgentArrow extends Spell {

	constructor() {
		super();
		this.id = "refulgent_arrow";
		this.name = "Refulgent Arrow";
		this.cast = 0;
		this.range = 25;
		this.recast = 0;
		this.potency = 330;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !!engine.hasBuff("straight_shot_ready");
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage)
		engine.removeBuff(engine.hasBuff("straight_shot_ready"));
	}

}