import StraightShotReady from "../../buffs/brd/StraightShotReady";
import StormbiteDebuff from "../../debuffs/brd/StormbiteDebuff";
import Engine from "../../Engine";
import Spell from "../Spell";

export default class Stormbite extends Spell {

	constructor() {
		super();
		this.id = "stormbite";
		this.name = "Stormbite";
		this.cast = 0;
		this.range = 25;
		this.recast = 0;
		this.potency = 100;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addDebuff(new StormbiteDebuff());
		if (Math.random() <= 0.35) {
			engine.addBuff(new StraightShotReady());
		}
	}

}