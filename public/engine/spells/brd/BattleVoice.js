import Spell from "../Spell";

export class BattleVoice extends Spell {

	constructor() {
		super();
		this.id = "battle_voice";
		this.name = "Battle Voice";
		this.cast = 0;
		this.offGcd = true;
		this.recast = 180 * 1000;
	}

}