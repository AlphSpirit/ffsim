import Spell from "../Spell";
import Engine from "../../Engine";

export default class ApexArrow extends Spell {

	constructor() {
		super();
		this.id = "apex_arrow";
		this.name = "Apex Arrow";
		this.range = 25;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("soul_gauge").amount >= 20;
	}

	/**
	 * @param {Engine} engine
	 */
	getPotency(engine) {
		// let multiplier = engine.resources.get("soul_gauge").amount / 100;
		// return Math.floor(600 * multiplier);
		return 600;
	}

	/**
	 * @param {Engine} engine
	 * @param {any} damage
	 */
	onCast(engine, damage) {
		engine.resources.get("soul_gauge").amount = 0;
	}

}