import Spell from "../Spell";

export default class Bloodletter extends Spell {

	constructor() {
		super();
		this.id = "bloodletter";
		this.name = "Bloodletter";
		this.cast = 0;
		this.offGcd = true;
		this.range = 25;
		this.recast = 15 * 1000;
		this.potency = 150;
	}

}