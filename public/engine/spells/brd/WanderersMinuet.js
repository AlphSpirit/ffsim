import Spell from "../Spell";
import WanderersMinuetBuff from "../../buffs/brd/WanderersMinuetBuff";

export default class WanderersMinuet extends Spell {

	constructor() {
		super();
		this.id = "wanderers_minuet";
		this.name = "The Wanderer's Minuet";
		this.cast = 0;
		this.offGcd = true;
		this.recast = 80 * 1000;
		this.potency = 100;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		let magesBallad = engine.hasBuff("mages_ballad");
		if (magesBallad) {
			engine.removeBuff(magesBallad);
		}
		let armysPaeon = engine.hasBuff("armys_paeon");
		if (armysPaeon) {
			engine.removeBuff(armysPaeon);
		}
		engine.addBuff(new WanderersMinuetBuff());
	}

}