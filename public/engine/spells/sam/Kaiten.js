import Spell from "../Spell";
import KaitenBuff from "../../buffs/sam/KaitenBuff";

export default class Kaiten extends Spell {

	constructor() {
		super();
		this.id = "kaiten";
		this.name = "Hissatsu: Kaiten";
		this.cast = 0;
		this.offGcd = true;
		this.range = 0;
		this.recast = 5 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("kenki").amount >= 20;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("kenki").add(-20);
		engine.addBuff(new KaitenBuff());
	}

}