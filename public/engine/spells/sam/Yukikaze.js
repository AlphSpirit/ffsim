import YukikazeDebuff from "../../debuffs/sam/YukikazeDebuff";
import Engine from "../../Engine";
import Spell from "../Spell";

export default class Yukikaze extends Spell {

	constructor() {
		super();
		this.id = "yukikaze";
		this.name = "Yukikaze";
		this.cast = 0;
		this.range = 3;
		this.recast = 0;
		this.potency = 360;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.lastComboAction == "hakaze" || !!engine.hasBuff("meikyo_shisui");
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		// engine.addDebuff(new YukikazeDebuff());
		engine.resources.get("kenki").add(15);
		engine.resources.get("setsu").add(1);
	}

}