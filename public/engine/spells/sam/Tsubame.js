import Spell from "../Spell";

export default class Tsubame extends Spell {

	constructor() {
		super();
		this.id = "tsubame";
		this.name = "Tsubame-gaeshi";
		this.range = 3;
		this.recast = 60 * 1000;
		this.potency = 1200;
		this.breaksCombos = false;
	}

	condition(engine) {
		return engine.lastAction == "midare";
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("meditation").add(1);
	}

}