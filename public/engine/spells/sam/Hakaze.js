import Spell from "../Spell";
import Engine from "../../Engine";

export default class Hakaze extends Spell {

	constructor() {
		super();
		this.id = "hakaze";
		this.name = "Hakaze";
		this.cast = 0;
		this.range = 3;
		this.recast = 0;
		this.potency = 200;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("kenki").add(5);
	}

}