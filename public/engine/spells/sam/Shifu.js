import ShifuBuff from "../../buffs/sam/ShifuBuff";
import Engine from "../../Engine";
import Spell from "../Spell";

export default class Shifu extends Spell {

	constructor() {
		super();
		this.id = "shifu";
		this.name = "Shifu";
		this.cast = 0;
		this.range = 3;
		this.recast = 0;
		this.potency = 320;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.lastComboAction == "hakaze";
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new ShifuBuff());
		engine.resources.get("kenki").add(5);
	}

}