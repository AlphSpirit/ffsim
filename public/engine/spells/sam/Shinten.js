import Spell from "../Spell";

export default class Shinten extends Spell {

	constructor() {
		super();
		this.id = "shinten";
		this.name = "Hissatsu: Shinten";
		this.cast = 0;
		this.offGcd = true;
		this.range = 3;
		this.potency = 320;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("kenki").amount >= 25;
	}

	/**
	 * @param {Engine} engine
	 */
	canClip(engine) {
		return !!engine.hasBuff("meikyo_shisui") && engine.resources.get("kenki").amount >= 35;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("kenki").add(-25);
	}

}