import Spell from "../Spell";

export default class Ikishoten extends Spell {

	constructor() {
		super();
		this.id = "ikishoten";
		this.name = "Ikishoten";
		this.cast = 0;
		this.offGcd = true;
		this.recast = 60 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("kenki").add(50);
	}

}