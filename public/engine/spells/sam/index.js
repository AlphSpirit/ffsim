import Gekko from "./Gekko";
import Senei from "./Senei";
import Hakaze from "./Hakaze";
import Higanbana from "./Higanbana";
import Ikishoten from "./Ikishoten";
import Jinpu from "./Jinpu";
import Kaiten from "./Kaiten";
import Kasha from "./Kasha";
import MeikyoShisui from "./MeikyoShisui";
import Midare from "./Midare";
import Shifu from "./Shifu";
import Shinten from "./Shinten";
import Tsubame from "./Tsubame";
import Yukikaze from "./Yukikaze";
import Shoha from "./Shoha";

export default [
	new Hakaze(),
	new Jinpu(),
	new Shifu(),
	new Kasha(),
	new Gekko(),
	new Yukikaze(),
	new Midare(),
	new Kaiten(),
	new Senei(),
	new Shinten(),
	new Higanbana(),
	new MeikyoShisui(),
	new Ikishoten(),
	new Tsubame(),
	new Shoha()
];