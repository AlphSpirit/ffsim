import Spell from "../Spell";
import Engine from "../../Engine";

export default class Shoha extends Spell {

	constructor() {
		super();
		this.id = "shoha";
		this.name = "Shoha";
		this.cast = 0;
		this.range = 3;
		this.recast = 15;
		this.potency = 400;
		this.offGcd = true;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("meditation").amount >= 3;
	}

	/**
	 * @param {Engine} engine
	 * @param {any} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("meditation").amount = 0;
	}

}