import Spell from "../Spell";

export default class Senei extends Spell {

	constructor() {
		super();
		this.id = "senei";
		this.name = "Hissatsu: Senei";
		this.cast = 0;
		this.offGcd = true;
		this.range = 10;
		this.recast = 60 * 2 * 1000;
		this.potency = 1100;
		this.breaksCombos = false;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("kenki").amount >= 50;
	}

	/**
	 * @param {Engine} engine
	 */
	canClip(engine) {
		return true;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("kenki").add(-50);
	}

}