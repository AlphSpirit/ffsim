import Engine from "../../Engine";
import Spell from "../Spell";

export default class Midare extends Spell {

	constructor() {
		super();
		this.id = "midare";
		this.name = "Midare Setsugekka";
		this.cast = 1.3 * 1000;
		this.range = 3;
		this.recast = 0;
		this.potency = 800;
		this.breaksCombos = false;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("ka").amount
			+ engine.resources.get("getsu").amount
			+ engine.resources.get("setsu").amount == 3;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("ka").amount = 0;
		engine.resources.get("getsu").amount = 0;
		engine.resources.get("setsu").amount = 0;
		engine.resources.get("meditation").add(1);
	}

}