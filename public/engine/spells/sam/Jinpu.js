import Spell from "../Spell";
import Engine from "../../Engine";
import JinpuBuff from "../../buffs/sam/JinpuBuff";

export default class Jinpu extends Spell {

	constructor() {
		super();
		this.id = "jinpu";
		this.name = "Jinpu";
		this.cast = 0;
		this.range = 3;
		this.recast = 0;
		this.potency = 320;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.lastComboAction == "hakaze";
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new JinpuBuff());
		engine.resources.get("kenki").add(5);
	}

}