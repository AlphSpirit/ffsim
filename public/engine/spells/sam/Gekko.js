import Spell from "../Spell";
import Engine from "../../Engine";

export default class Gekko extends Spell {

	constructor() {
		super();
		this.id = "gekko";
		this.name = "Gekko";
		this.cast = 0;
		this.range = 3;
		this.recast = 0;
		this.potency = 480;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.lastComboAction == "jinpu" || !!engine.hasBuff("meikyo_shisui");
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.resources.get("kenki").add(10);
		engine.resources.get("getsu").add(1);
	}

}