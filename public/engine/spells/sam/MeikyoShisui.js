import Spell from "../Spell";
import MeikyoShisuiBuff from "../../buffs/sam/MeikyoShisuiBuff";

export default class MeikyoShisui extends Spell {

	constructor() {
		super();
		this.id = "meikyo_shisui";
		this.name = "Meikyo Shisui";
		this.cast = 0;
		this.offGcd = true;
		this.recast = 80 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addBuff(new MeikyoShisuiBuff());
	}

}