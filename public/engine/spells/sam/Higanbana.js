import Spell from "../Spell";
import HiganbanaDebuff from "../../debuffs/sam/HiganbanaDebuff";

export default class Higanbana extends Spell {

	constructor() {
		super();
		this.id = "higanbana";
		this.name = "Higanbana";
		this.cast = 1.8 * 1000;
		this.range = 3;
		this.recast = 0;
		this.recastGcd = true;
		this.potency = 250;
		this.breaksCombos = false;
	}

	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return engine.resources.get("ka").amount
			+ engine.resources.get("getsu").amount
			+ engine.resources.get("setsu").amount == 1;
	}

	/**
	 * @param {Engine} engine
	 * @param {{damage: number, criticalHit: boolean, directHit: boolean}} damage
	 */
	onCast(engine, damage) {
		super.onCast(engine, damage);
		engine.addDebuff(new HiganbanaDebuff());
		engine.resources.get("ka").amount = 0;
		engine.resources.get("getsu").amount = 0;
		engine.resources.get("setsu").amount = 0;
		engine.resources.get("meditation").add(1);
	}

}