import Engine from "../../Engine";
import Debuff from "../Debuff";

export default class StormbiteDebuff extends Debuff {

	constructor() {
		super();
		this.id = "stormbite";
		this.name = "Stormbite";
		this.isRefreshing = true;
		this.potency = 50;
		this.duration = 30 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	tick(engine) {
		let damage = super.tick(engine);
		if (damage.criticalHit) {
			if (engine.hasBuff("mages_ballad")) {
				engine.spells.get("bloodletter").cooldown = 0;
				engine.resources.get("soul_gauge").add(5);
			} else if (engine.hasBuff("wanderers_minuet")) {
				engine.resources.get("repertoire_minuet").add(1);
				engine.resources.get("soul_gauge").add(5);
			} else if (engine.hasBuff("armys_paeon")) {
				engine.resources.get("repertoire_paeon").add(1);
				engine.resources.get("soul_gauge").add(5);
			}
		}
		return damage;
	}

}