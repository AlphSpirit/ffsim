import Debuff from "../Debuff";

export default class CausticBiteDebuff extends Debuff {

	constructor() {
		super();
		this.id = "caustic_bite";
		this.name = "Caustic Bite";
		this.isRefreshing = true;
		this.potency = 40;
		this.duration = 30 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	tick(engine) {
		let damage = super.tick(engine);
		if (Math.random() <= 0.4) {
			if (engine.hasBuff("mages_ballad")) {
				engine.spells.get("bloodletter").cooldown = 0;
				engine.resources.get("soul_gauge").add(5);
			} else if (engine.hasBuff("wanderers_minuet")) {
				engine.resources.get("repertoire_minuet").add(1);
				engine.resources.get("soul_gauge").add(5);
			} else if (engine.hasBuff("armys_paeon")) {
				engine.resources.get("repertoire_paeon").add(1);
				engine.resources.get("soul_gauge").add(5);
			}
		}
		return damage;
	}

}