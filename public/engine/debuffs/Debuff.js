import Buff from "../buffs/Buff";
import BuffEffect from "../buffs/BuffEffect";
import Engine from "../Engine";
import Spell from "../spells/Spell";

export default class Debuff {

	constructor() {
		this.id = "";
		this.name = "";
		this.isRefreshing = false;
		this.duration = 0;
		this.remaining = 0;
		this.potency = 0;
		this.snapshotBuffs = /** @type {Buff[]} */ ([]);
	}

	/**
	 * @param {Engine} engine
	 * @param {Spell|Debuff} spell
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, spell, buffEffect) {}

	/**
	 * @param {string} id
	 */
	hasBuff(id) {
		for (let buff of this.snapshotBuffs) {
			if (buff.id == id) {
				return buff;
			}
		}
		return null;
	}

	/**
	 * @param {Engine} engine
	 */
	tick(engine) {
		if (this.potency) {
			return engine.computeDotDamage(this, this.snapshotBuffs);
		}
		return null;
	};

	/**
	 * @param {Buff[]} activeBuffs
	 * @param {Debuff} [debuff]
	 */
	refresh(activeBuffs, debuff) {
		this.snapshotBuffs = activeBuffs.slice();
		this.remaining = this.duration;
	}

}