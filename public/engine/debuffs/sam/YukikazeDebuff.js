import BuffEffect from "../../buffs/BuffEffect";
import Spell from "../../spells/Spell";
import Debuff from "../Debuff";

export default class YukikazeDebuff extends Debuff {

	constructor() {
		super();
		this.id = "yukikaze";
		this.name = "Yukikaze";
		this.isRefreshing = true;
		this.duration = 30 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {Spell} spell
	 * @param {BuffEffect} buffEffect
	 * 
	 */
	applyEffect(engine, spell, buffEffect) {
		buffEffect.damageMultiplier *= 1.1;
	}

}