import Debuff from "../Debuff";

export default class HiganbanaDebuff extends Debuff {

	constructor() {
		super();
		this.id = "higanbana";
		this.name = "Higanbana";
		this.isRefreshing = true;
		this.potency = 40;
		this.duration = 60 * 1000;
	}

}