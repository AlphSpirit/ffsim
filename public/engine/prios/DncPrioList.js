import PriorityList from "./PriorityList";
import Engine from "../Engine";

let DncPrioList = new PriorityList("dnc", [{
	id: "technical_step"
}, {
	id: "technical_finish"
}, {
	id: "standard_step"
}, {
	id: "standard_finish"
}, {
	id: "emboite"
}, {
	id: "devilment"
}, {
	id: "flourish",
	/**
	 * @param {Engine} engine
	 */
	condition(engine) {
		return !engine.hasBuff("flourishing_cascade")
				&& !engine.hasBuff("flourishing_fountain")
				&& !engine.hasBuff("flourishing_fan_dance");
	}
}, {
	id: "fan_dance_iii"
}, {
	id: "fan_dance"
}, {
	id: "saber_dance"
}, {
	id: "fountainfall"
}, {
	id: "reverse_cascade"
}, {
	id: "bloodshower"
}, {
	id: "rising_windmill"
}, {
	id: "fountain"
}, {
	id: "cascade"
}]);

export default DncPrioList;