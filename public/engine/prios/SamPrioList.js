import PriorityList from "./PriorityList";

let prio = new PriorityList("sam", [{
	id: "kaiten",
	condition(engine) {
		return engine.spells.get("midare").condition(engine)
			|| (engine.spells.get("higanbana").condition(engine)
				&& engine.hasBuff("jinpu")
				&& mustRefreshHiganbana(engine));
	}
}, {
	id: "meikyo_shisui",
	condition(engine) {
		let shifu = engine.hasBuff("shifu")
		let jinpu = engine.hasBuff("jinpu");
		return shifu && jinpu && shifu.remaining >= 10 * 1000 && jinpu.remaining > 10 * 1000
			&& engine.lastComboAction != "jinpu";
	}
}, {
	id: "higanbana",
	condition(engine) {
		return engine.hasBuff("kaiten")
			&& engine.hasBuff("jinpu")
			&& mustRefreshHiganbana(engine);
	}
}, {
	id: "midare"
}, {
	id: "tsubame"
}, {
	id: "senei",
	condition(engine) {
		return !!engine.hasBuff("jinpu");
	}
}, {
	id: "shinten",
	condition(engine) {
		return (engine.resources.get("kenki").amount >= 45 && engine.spells.get("senei").cooldown > 0);
	}
}, {
	id: "shifu",
	condition(engine) {
		return !engine.hasBuff("shifu") || engine.hasBuff("shifu").remaining < 10 * 1000;
	}
}, {
	id: "gekko",
	condition(engine) {
		return engine.resources.get("getsu").amount == 0;
	}
}, {
	id: "jinpu",
	condition(engine) {
		return !engine.hasBuff("jinpu") || engine.hasBuff("jinpu").remaining < 10 * 1000;
	}
}, {
	id: "yukikaze",
	condition(engine) {
		return !engine.hasDebuff("yukikaze") || engine.hasDebuff("yukikaze").remaining < 10 * 1000;
	}
}, {
	id: "kasha",
	condition(engine) {
		return engine.resources.get("ka").amount == 0;
	}
}, {
	id: "shifu",
	condition(engine) {
		return engine.resources.get("ka").amount == 0;
	}
}, {
	id: "jinpu",
	condition(engine) {
		return engine.resources.get("getsu").amount == 0;
	}
}, {
	id: "yukikaze",
	condition(engine) {
		return engine.resources.get("setsu").amount == 0;
	}
},{
	id: "shoha",
}, {
	id: "hakaze"
}, {
	id: "ikishoten"
}]);

/**
 * @param {Engine} engine
 */
function mustRefreshHiganbana(engine) {
	// return !engine.hasDebuff("higanbana") || engine.hasDebuff("higanbana").remaining <= 20 * 1000;
	return !engine.hasDebuff("higanbana");
}

export default prio;