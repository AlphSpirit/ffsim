import PriorityList from "./PriorityList";

let prio = new PriorityList("brd", [{
	id: "stormbite",
	condition(engine) {
		let hasDebuff = engine.hasDebuff("stormbite");
		return !hasDebuff;
	}
}, {
	id: "caustic_bite",
	condition(engine) {
		let hasDebuff = engine.hasDebuff("caustic_bite");
		return !hasDebuff;
	}
}, {
	id: "iron_jaws",
	condition(engine) {
		let stormbite = engine.hasDebuff("stormbite");
		let causticBite = engine.hasDebuff("caustic_bite");
		return (stormbite && causticBite
			&& (stormbite.remaining <= engine.stats.getBaseGcd(engine)
			|| causticBite.remaining <= engine.stats.getBaseGcd(engine)));
	}
}, {
	id: "bloodletter",
	condition(engine) {
		return !engine.hasBuff("barrage");
	}
},{
	id: "raging_strikes"
}, {
	id: "wanderers_minuet",
	condition(engine) {
		return !!engine.hasBuff("raging_strikes") || engine.spells.get("raging_strikes").cooldown <= 0;
	}
}, {
	id: "mages_ballad",
	condition(engine) {
		let wanderersMinuet = engine.hasBuff("wanderers_minuet");
		return !wanderersMinuet || wanderersMinuet.remaining < engine.stats.getBaseGcd(engine);
	}
}, {
	id: "armys_paeon",
	condition(engine) {
		let wanderersMinuet = engine.hasBuff("wanderers_minuet");
		let magesBallad = engine.hasBuff("mages_ballad");
		return (!wanderersMinuet || wanderersMinuet.remaining < engine.stats.getBaseGcd(engine))
			&& (!magesBallad || magesBallad.remaining < engine.stats.getBaseGcd(engine));
	}
}, {
	id: "iron_jaws",
	condition(engine) {
		return needToRefreshDOTs(engine);
	}
}, {
	id: "refulgent_arrow"
}, {
	id: "empyreal_arrow",
	condition(engine) {
		return !engine.hasBuff("barrage");
	}
}, {
	id: "battle_voice"
}, {
	id: "barrage",
	condition(engine) {
		return !needToRefreshDOTs(engine) && !engine.hasBuff("straight_shot_ready");
	}
}, {
	id: "sidewinder",
	condition(engine) {
		let stormbite = engine.hasDebuff("stormbite");
		let causticBite = engine.hasDebuff("caustic_bite");
		return !!stormbite && !!causticBite;
	}
}, {
	id: "pitch_perfect",
	condition(engine) {
		let stacks = engine.resources.get("repertoire_minuet").amount;
		let minuet = engine.hasBuff("wanderers_minuet");
		return minuet && (stacks == 3 || minuet.remaining < engine.stats.getBaseGcd(engine));
	}
}, {
	id: "apex_arrow",
	condition(engine) {
		return engine.resources.get("soul_gauge").amount >= 100;
	}
}, {
	id: "burst_shot"
}]);

let needToRefreshDOTs = function(engine) {
	let stormbite = engine.hasDebuff("stormbite");
	let causticBite = engine.hasDebuff("caustic_bite");
	let ragingStrikes = engine.hasBuff("raging_strikes");
	return (stormbite && causticBite
		&& ((ragingStrikes && ragingStrikes.remaining <= engine.stats.getBaseGcd(engine))
			|| (ragingStrikes && (!stormbite.hasBuff("raging_strikes") || !causticBite.hasBuff("raging_strikes")))
		));
}

export default prio;