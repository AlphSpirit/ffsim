import Engine from "../Engine";
import Spell from "../spells/Spell";

/**
 * @typedef PriorityListElement
 * @property {string} id
 * @property {function(Engine): boolean} [condition]
 */

export default class PriorityList {

	/**
	 * @param {string} className
	 * @param {PriorityListElement[]} prioList
	 */
	constructor(className, prioList) {
		this.prioList = prioList;
		this.class = className;
	}

	/**
	 * @param {Engine} engine
	 * @returns {Spell}
	 */
	getNextSpell(engine) {
		for (let element of this.prioList) {
			let spell = engine.spells.get(element.id);
			if (!spell.offGcd && spell.cooldown <= 0 && spell.condition(engine) && (!element.condition || element.condition(engine))) {
				return spell;
			}
		}
	}

		/**
	 * @param {Engine} engine
	 * @returns {Spell}
	 */
	getNextInstantSpell(engine) {
		for (let element of this.prioList) {
			let spell = engine.spells.get(element.id);
			if (!spell) {
				console.log(element.id);
				continue;
			}
			if (spell.offGcd && spell.cooldown <= 0 && spell.condition(engine) && (!element.condition || element.condition(engine))) {
				return spell;
			}
		}
	}

	/**
	 * @param {Engine} engine
	 * @returns {Spell[]}
	 */
	getAllSpells(engine) {
		let spells = [];
		for (let element of this.prioList) {
			let spell = engine.spells.get(element.id);
			if (!spell) {
				console.log(element.id);
				continue;
			}
			if (spells.indexOf(spell) == -1) {
				spells.push(spell);
			}
		}
		return spells;
	}

}