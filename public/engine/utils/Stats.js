import Engine from "../Engine";
import BuffEffect from "../buffs/BuffEffect";

export default class Stats {

	constructor() {

		this.strength = 0;
		this.dexterity = 0;
		this.intelligence = 0;
		this.criticalHit = 0;
		this.directHit = 0;
		this.determination = 0;
		this.skillSpeed = 0;
		this.spellSpeed = 0;
		this.weaponDamage = 0;
		this.weaponDelay = 0;

		this.lvl = 80;
		this.lvlMain = 340;
		this.lvlSub = 280;
		this.lvlDiv = 3300;

		this.className = null;
		this.attackPower = 0;
		this.jobMod = 0;
		this.autoAttackMultiplier = 0;
		this.weaponDamageMultiplier = 0;
		this.attackPowerMultiplier = 0;
		this.determinationMultiplier = 0;
		this.dotSkillSpeedMultiplier = 0;

		this.jobMods = {brd: 115, sam: 113, dnc: 115};

	}

	/**
	 * @param {*} stats
	 * @param {string} className
	 */
	load(stats, className) {

		this.strength = parseInt(stats.strength);
		this.dexterity = parseInt(stats.dexterity);
		this.intelligence = parseInt(stats.intelligence);
		this.criticalHit = parseInt(stats.criticalHit);
		this.directHit = parseInt(stats.directHit);
		this.determination = parseInt(stats.determination);
		this.skillSpeed = parseInt(stats.skillSpeed);
		this.spellSpeed = parseInt(stats.spellSpeed);
		this.weaponDamage = parseInt(stats.weaponDamage);
		this.weaponDelay = parseFloat(stats.weaponDelay);

		this.className = className;
		this.jobMod = this.jobMods[this.className];
		this.calculateModifiers();

	}

	calculateModifiers() {
		if (this.className == "brd" || this.className == "dnc") {
			this.attackPower = this.dexterity;
		} else if (this.className == "sam") {
			this.attackPower = this.strength;
		}
		this.autoAttackMultiplier = Math.floor(Math.floor(this.lvlMain * this.jobMod / 1000 + this.weaponDamage) * this.weaponDelay / 3);
		this.weaponDamageMultiplier = Math.floor(this.lvlMain * this.jobMod / 1000 + this.weaponDamage);
		this.attackPowerMultiplier = (100 + Math.floor(165 * (this.attackPower - this.lvlMain) / this.lvlMain)) / 100;
		this.determinationMultiplier = Math.floor(130 * (this.determination - this.lvlMain) / this.lvlDiv + 1000) / 1000;
		this.dotSkillSpeedMultiplier = Math.floor(130 * (this.skillSpeed - this.lvlSub) / this.lvlDiv + 1000) / 1000;
	}

	getAttackPower() {
		return this.attackPower;
	}

	/**
	 * @param {Engine} engine
	 */
	getBaseGcd(engine) {
		return this.getBaseGcdMultiplier(engine) * 2.5 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	getBaseGcdMultiplier(engine) {
		let buffEffect = new BuffEffect();
		for (let buff of engine.buffs) {
			buff.applyToStats(engine, buffEffect);
		}
		let base = 1 / (Math.floor(130 * (this.skillSpeed - 380) / 3300 + 1000) / 1000);
		base *= (1 - buffEffect.haste / 100);
		return base;
	}

}