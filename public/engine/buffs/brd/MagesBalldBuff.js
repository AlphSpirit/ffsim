import Buff from "../Buff";

export default class MagesBalladBuff extends Buff {

	constructor() {
		super();
		this.id = "mages_ballad";
		this.name = "Mage's Ballad";
		this.duration = 30 * 1000;
	}

}