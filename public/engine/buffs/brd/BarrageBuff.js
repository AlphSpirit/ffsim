import Buff from "../Buff";
import Engine from "../../Engine";
import Spell from "../../spells/Spell";
import BuffEffect from "../BuffEffect";
import RefulgentArrow from "../../spells/brd/RefulgentArrow";
import EmpyrealArrow from "../../spells/brd/EmpyrealArrow";

export default class BarrageBuff extends Buff {

	constructor() {
		super();
		this.id = "barrage";
		this.name = "Barrage";
		this.duration = 10 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {Spell} spell
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, spell, buffEffect) {
		if (spell && (spell.id == "refulgent_arrow"
			|| spell.id == "empyreal_arrow")) {
			buffEffect.damageMultiplier *= 3;
			engine.removeBuff(this);
		}
	}

}