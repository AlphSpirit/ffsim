import Buff from "../Buff";

export default class ArmysMuseBuff extends Buff {

	/**
	 * @param {number} repertoire
	 */
	constructor(repertoire) {
		super();
		this.id = "armys_muse";
		this.name = "Army's Muse";
		this.duration = 10 * 1000;
		this.repertoire = repertoire;
	}

	applyToStats(engine, buffEffect) {
		switch (this.repertoire) {
			case 1:
				buffEffect.haste += 1;
				break;
			case 2:
				buffEffect.haste += 2;
				break;
			case 3:
				buffEffect.haste += 4;
				break;
			case 4:
				buffEffect.haste += 12;
				break;
		}
	}

}