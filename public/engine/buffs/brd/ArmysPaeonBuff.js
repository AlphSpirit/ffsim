import Buff from "../Buff";
import Engine from "../../Engine";
import BuffEffect from "../BuffEffect";
import ArmysMuseBuff from "./ArmysMuseBuff";

export default class ArmysPaeonBuff extends Buff {

	constructor() {
		super();
		this.id = "armys_paeon";
		this.name = "Army's Paeon";
		this.duration = 30 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {BuffEffect} buffEffect
	 */
	applyToStats(engine, buffEffect) {
		buffEffect.haste += 4 * engine.resources.get("repertoire_paeon").amount;
	}

	/**
	 * @param {Engine} engine
	 */
	onExpire(engine) {
		engine.addBuff(new ArmysMuseBuff(engine.resources.get("repertoire_paeon").amount));
		engine.resources.get("repertoire_paeon").amount = 0;
	}

}