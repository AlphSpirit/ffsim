import Buff from "../Buff";

export default class StraightShotReady extends Buff {

	constructor() {
		super();
		this.id = "straight_shot_ready";
		this.name = "Straight Shot Ready";
		this.duration = 10 * 1000;
		this.isRefreshing = true;
	}

}