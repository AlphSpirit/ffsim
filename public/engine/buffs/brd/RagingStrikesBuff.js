import Engine from "../../Engine";
import Spell from "../../spells/Spell";
import Buff from "../Buff";
import BuffEffect from "../BuffEffect";

export class RagingStrikesBuff extends Buff {

	constructor() {
		super();
		this.id = "raging_strikes";
		this.name = "Raging Strikes";
		this.duration = 20 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {Spell} spell
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, spell, buffEffect) {
		buffEffect.damageMultiplier *= 1.1;
	}

}