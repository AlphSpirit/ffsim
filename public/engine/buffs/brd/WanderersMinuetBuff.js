import Buff from "../Buff";
import Engine from "../../Engine";

export default class WanderersMinuetBuff extends Buff {

	constructor() {
		super();
		this.id = "wanderers_minuet";
		this.name = "The Wanderer's Minuet";
		this.duration = 30 * 1000;
	}

	/**
	 * @param {Engine} engine
	 */
	onExpire(engine) {
		engine.resources.get("repertoire_minuet").amount = 0;
	}

}