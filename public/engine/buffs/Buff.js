import Debuff from "../debuffs/Debuff";
import Spell from "../spells/Spell";
import BuffEffect from "./BuffEffect";

export default class Buff {

	constructor() {
		this.id = "";
		this.name = "";
		this.isRefreshing = false;
		this.duration = 0;
		this.remaining = 0;
	}

	/**
	 * @param {Engine} engine
	 * @param {Spell|Debuff} spell
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, spell, buffEffect) {}

	/**
	 * @param {Engine} engine
	 * @param {Debuff} debuff
	 * @param {BuffEffect} buffEffect
	 */
	applyToDebuff(engine, debuff, buffEffect) {}

	/**
	 * @param {Engine} engine
	 * @param {BuffEffect} buffEffect
	 */
	applyToStats(engine, buffEffect) {}

	/**
	 * @param {Buff} [buff]
	 */
	refresh(buff) {
		if (buff) {
			this.remaining = Math.max(this.duration, buff.duration);
		} else {
			this.remaining = this.duration;
		}
	}

	/**
	 * @param {Engine} engine
	 */
	onExpire(engine) {}

}