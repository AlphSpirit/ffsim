import Engine from "../../Engine";
import Spell from "../../spells/Spell";
import Buff from "../Buff";
import BuffEffect from "../BuffEffect";
import Debuff from "../../debuffs/Debuff";

export default class KaitenBuff extends Buff {

	constructor() {
		super();
		this.id = "kaiten";
		this.name = "Hissatsu: Kaiten";
		this.duration = 10 * 1000;
		this.isRefreshing = true;
	}

	/**
	 * @param {Engine} engine
	 * @param {Spell} spell
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, spell, buffEffect) {
		if (spell && (spell.id != "higanbana" && spell.id != "tsubame" && spell.id != "senei")) {
			buffEffect.potency *= 1.5;
			engine.removeBuff(this);
		}
	}

	/**
	 * @param {Engine} engine
	 * @param {Debuff} debuff
	 * @param {BuffEffect} buffEffect
	 */
	applyToDebuff(engine, debuff, buffEffect) {
		if (debuff.id == "higanbana") {
			buffEffect.potency *= 1.5;
			engine.removeBuff(this);
		}
	}

}