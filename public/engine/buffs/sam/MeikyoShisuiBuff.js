import Buff from "../Buff";
import Spell from "../../spells/Spell";
import Engine from "../../Engine";
import BuffEffect from "../BuffEffect";

export default class MeikyoShisuiBuff extends Buff {

	constructor() {
		super();
		this.id = "meikyo_shisui";
		this.name = "Meikyo Shisui";
		this.duration = 15 * 1000;
		this.stacks = 3;
	}

	/**
	 * @param {Engine} engine
	 * @param {Spell} spell
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, spell, buffEffect) {
		if (spell && (spell.id == "gekko" || spell.id == "kasha" || spell.id == "yukikaze")) {
			this.stacks--;
			if (this.stacks <= 0) {
				engine.removeBuff(this);
			}
		}
	}

}