import Buff from "../Buff";
import BuffEffect from "../BuffEffect";
import Spell from "../../spells/Spell";

export default class ShifuBuff extends Buff {

	constructor() {
		super();
		this.id = "shifu";
		this.name = "Shifu";
		this.duration = 40 * 1000;
		this.isRefreshing = true;
	}

	/**
	 * @param {Engine} engine
	 * @param {BuffEffect} buffEffect
	 */
	applyToStats(engine, buffEffect) {
		buffEffect.haste += 13;
	}

}