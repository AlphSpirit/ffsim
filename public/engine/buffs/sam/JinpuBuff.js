import Buff from "../Buff";
import BuffEffect from "../BuffEffect";
import Spell from "../../spells/Spell";

export default class JinpuBuff extends Buff {

	constructor() {
		super();
		this.id = "jinpu";
		this.name = "Jinpu";
		this.duration = 40 * 1000;
		this.isRefreshing = true;
	}

	/**
	 * @param {Engine} engine
	 * @param {Spell} spell
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, spell, buffEffect) {
		buffEffect.damageMultiplier *= 1.13;
	}

}