export default class BuffEffect {

	constructor() {
		this.potency = 0;
		this.critChanceFlat = 0;
		this.dhChanceFlat = 0;
		this.damageMultiplier = 1;
		this.haste = 0;
	}

}