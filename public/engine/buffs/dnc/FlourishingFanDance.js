import Buff from "../Buff";

export default class FlourishingFanDance extends Buff {

	constructor() {
		super();
		this.id = "flourishing_fan_dance";
		this.name = "Flourishing Fan Dance";
		this.duration = 15 * 1000;
		this.isRefreshing = true;
	}

}