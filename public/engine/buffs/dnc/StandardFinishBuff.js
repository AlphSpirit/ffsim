import Buff from "../Buff";
import Engine from "../../Engine";
import BuffEffect from "../BuffEffect";

export default class StandardFinishBuff extends Buff {

	/**
	 * @param {number} duration
	 */
	constructor(duration) {
		super();
		this.id = "standard_finish";
		this.name = "Standard Finish";
		this.duration = duration;
		this.isRefreshing = true;
	}

	/**
	 * @param {Engine} engine
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, _, buffEffect) {
		buffEffect.damageMultiplier *= 1.05;
	}

}