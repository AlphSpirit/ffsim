import Buff from "../Buff";

export default class TechnicalStepBuff extends Buff {

	constructor() {
		super();
		this.id = "technical_step";
		this.name = "Technical Step";
		this.duration = 15 * 1000;
	}

}