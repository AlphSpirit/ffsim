import Buff from "../Buff";

export default class StandardStepBuff extends Buff {

	constructor() {
		super();
		this.id = "standard_step";
		this.name = "Standard Step";
		this.duration = 15 * 1000;
	}

}