import Buff from "../Buff";

export default class FlourishingFountain extends Buff {

	constructor() {
		super();
		this.id = "flourishing_fountain";
		this.name = "Flourishing Fountain";
		this.duration = 20 * 1000;
		this.isRefreshing = true;
	}

}