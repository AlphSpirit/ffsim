import Buff from "../Buff";

export default class FlourishingCascade extends Buff {

	constructor() {
		super();
		this.id = "flourishing_cascade";
		this.name = "Flourishing Cascade";
		this.duration = 20 * 1000;
		this.isRefreshing = true;
	}

}