import Buff from "../Buff";

export default class FlourishingWindmill extends Buff {

	constructor() {
		super();
		this.id = "flourishing_windmill";
		this.name = "Flourishing Windmill";
		this.duration = 20 * 1000;
		this.isRefreshing = true;
	}

}