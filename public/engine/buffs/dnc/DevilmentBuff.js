import Buff from "../Buff";
import Engine from "../../Engine";
import BuffEffect from "../BuffEffect";

export default class DevilmentBuff extends Buff {

	constructor() {
		super();
		this.id = "devilment";
		this.name = "Devilment";
		this.duration = 20 * 1000;
	}

	/**
	 * @param {Engine} engine
	 * @param {BuffEffect} buffEffect
	 */
	applyEffect(engine, _, buffEffect) {
		buffEffect.critChanceFlat += 0.2;
		buffEffect.dhChanceFlat += 0.2;
	}

}