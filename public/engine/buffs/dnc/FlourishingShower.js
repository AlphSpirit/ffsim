import Buff from "../Buff";

export default class FlourishingShower extends Buff {

	constructor() {
		super();
		this.id = "flourishing_shower";
		this.name = "Flourishing Shower";
		this.duration = 20 * 1000;
		this.isRefreshing = true;
	}

}