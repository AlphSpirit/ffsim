import Resource from "./Resource";

export default class Ka extends Resource {

	constructor() {
		super();
		this.id = "ka";
		this.name = "Ka";
		this.max = 1;
	}

}