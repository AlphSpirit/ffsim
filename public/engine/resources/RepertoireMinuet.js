import Resource from "./Resource";

export default class RepertoireMinuet extends Resource {

	constructor() {
		super();
		this.id = "repertoire_minuet";
		this.name = "Repertoire (Wanderer's Minuet)";
		this.max = 3;
	}

}