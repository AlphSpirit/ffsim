import Resource from "./Resource";

export default class FourfoldFeathers extends Resource {

	constructor() {
		super();
		this.id = "fourfold_feathers";
		this.name = "Fourfold Feathers";
		this.max = 4;
	}

}