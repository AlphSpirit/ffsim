import Resource from "./Resource";

export default class Kenki extends Resource {

	constructor() {
		super();
		this.id = "kenki";
		this.name = "Kenki";
		this.max = 100;
	}

}