import Resource from "./Resource";

export default class Meditation extends Resource {

	constructor() {
		super();
		this.id = "meditation";
		this.name = "Meditation";
		this.max = 3;
	}

}