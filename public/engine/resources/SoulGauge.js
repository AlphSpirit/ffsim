import Resource from "./Resource";

export default class SoulGauge extends Resource {

	constructor() {
		super();
		this.id = "soul_gauge";
		this.name = "Soul Gauge";
		this.max = 100;
	}

}