import Resource from "./Resource";

export default class Getsu extends Resource {

	constructor() {
		super();
		this.id = "getsu";
		this.name = "Getsu";
		this.max = 1;
	}

}