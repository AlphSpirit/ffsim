import Resource from "./Resource";

export default class RepertoirePaeon extends Resource {

	constructor() {
		super();
		this.id = "repertoire_paeon";
		this.name = "Repertoire (Army's Paeon)";
		this.max = 4;
	}

}