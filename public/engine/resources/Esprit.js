import Resource from "./Resource";

export default class Esprit extends Resource {

	constructor() {
		super();
		this.id = "esprit";
		this.name = "Esprit";
		this.max = 100;
	}

}