import Esprit from "./Esprit";
import FourfoldFeathers from "./FourfoldFeathers";
import Getsu from "./Getsu";
import Ka from "./Ka";
import Kenki from "./Kenki";
import RepertoireMinuet from "./RepertoireMinuet";
import RepertoirePaeon from "./RepertoirePaeon";
import Setsu from "./Setsu";
import SoulGauge from "./SoulGauge";
import Steps from "./Steps";
import Meditation from "./Meditation";

export default [

	new RepertoireMinuet(),
	new RepertoirePaeon(),
	new SoulGauge(),

	new Kenki(),
	new Ka(),
	new Getsu(),
	new Setsu(),
	new Meditation(),

	new FourfoldFeathers(),
	new Steps(),
	new Esprit()

];