import Resource from "./Resource";

export default class Setsu extends Resource {

	constructor() {
		super();
		this.id = "setsu";
		this.name = "Setsu";
		this.max = 1;
	}

}