export default class Resource {

	constructor() {
		this.id = "";
		this.name = "";
		this.amount = 0;
		this.min = 0;
		this.max = 0;
	}

	add(amount) {
		this.amount += amount;
		this.amount = Math.max(Math.min(this.amount, this.max), this.min);
	}

}