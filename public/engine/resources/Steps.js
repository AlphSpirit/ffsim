import Resource from "./Resource";

export default class Steps extends Resource {

	constructor() {
		super();
		this.id = "steps";
		this.name = "steps";
		this.max = 4;
	}

}