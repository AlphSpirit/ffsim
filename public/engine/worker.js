importScripts("ffsim.js");

let engine = new Engine();

onmessage = e => {

	if (e.data.type == "start") {
		engine.start(e.data, response => {
			this.postMessage(response);
		});
	}

}