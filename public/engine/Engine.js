import Buff from "./buffs/Buff";
import BuffEffect from "./buffs/BuffEffect";
import Debuff from "./debuffs/Debuff";
import BrdPrioList from "./prios/BrdPrioList";
import DncPrioList from "./prios/DncPrioList";
import PriorityList from "./prios/PriorityList";
import SamPrioList from "./prios/SamPrioList";
import Resources from "./resources/index";
import Resource from "./resources/Resource";
import BardSpells from "./spells/brd/index";
import DncSpells from "./spells/dnc/DncSpells";
import SamuraiSpells from "./spells/sam/index";
import Spell from "./spells/Spell";
import Stats from "./utils/Stats";

export default class Engine {

	constructor() {
		this.stats = new Stats();
		this.spellsArray = [
			...BardSpells,
			...SamuraiSpells,
			...DncSpells
		];
		this.spells = /** @type {Map<string, Spell>} */ (new Map());
		for (let spell of this.spellsArray) {
			this.spells.set(spell.id, spell);
		}
		this.resources = /** @type {Map<string, Resource>} */ (new Map());
		for (let resource of [...Resources]) {
			this.resources.set(resource.id, resource);
		}
		this.prioLists = [
			BrdPrioList,
			SamPrioList,
			DncPrioList
		];
		this.className = "";
		this.buffs = /** @type {Buff[]} */ ([]);
		this.debuffs = /** @type {Debuff[]} */ ([]);
		this.duration = 8 * 60 * 1000; // 8 minutes
		this.currentTime = 0;
		this.iterations = 100;
		this.lastAction = "";
		this.lastComboAction = "";
		this.damagePassives = {brd: 1.2, sam: 1, dnc: 1.2};
	}

	reset() {
		this.buffs = /** @type {Buff[]} */ ([]);
		this.debuffs = /** @type {Debuff[]} */ ([]);
		this.currentTime = 0;
		this.lastAction = "";
		this.lastComboAction = "";
		for (let spell of this.spells.values()) {
			spell.cooldown = 0;
		}
		for (let resource of this.resources.values()) {
			resource.amount = 0;
		}
	}

	/**
	 * @param {Buff} buff
	 */
	addBuff(buff) {
		let hasBuff = this.hasBuff(buff.id);
		if (hasBuff && buff.isRefreshing) {
			hasBuff.refresh(buff);
			return;
		}
		buff.refresh();
		this.buffs.push(buff);
	}

	/**
	 * @param {string} id
	 * @returns {Buff}
	 */
	hasBuff(id) {
		for (let buff of this.buffs) {
			if (buff.id == id) {
				return buff;
			}
		}
	}

	/**
	 * @param {Buff} buff
	 */
	removeBuff(buff) {
		let index = this.buffs.indexOf(buff);
		if (index >= -1) {
			this.buffs.splice(index, 1);
			buff.onExpire(this);
		}
	}

	/**
	 * @param {Debuff} debuff
	 */
	addDebuff(debuff) {
		let hasDebuff = this.hasDebuff(debuff.id);
		if (hasDebuff && debuff.isRefreshing) {
			this.debuffs.splice(this.debuffs.indexOf(hasDebuff), 1);
		}
		let buffEffect = new BuffEffect();
		buffEffect.potency = debuff.potency;
		for (let buff of this.buffs) {
			buff.applyToDebuff(this, debuff, buffEffect);
		}
		debuff.potency = buffEffect.potency;
		debuff.refresh(this.buffs);
		this.debuffs.push(debuff);
	}

	/**
	 * @param {string} id
	 * @returns {Debuff}
	 */
	hasDebuff(id) {
		for (let debuff of this.debuffs) {
			if (debuff.id == id) {
				return debuff;
			}
		}
	}

	getLifeRemaining() {
		return (1 - this.currentTime / this.duration) * 100;
	}

	/**
	 * @param {any} options
	 * @param {Function} callback
	 */
	start(options, callback) {

		let before = Date.now();
		console.log("STARTING SIMULATION ENGINE...");

		if (options.iterations) {
			this.iterations = options.iterations;
		}
		this.className = options.class;
		this.stats.load(options.stats, this.className);

		// Add all stats to weight
		let toWeight = [];
		for (let key of Object.keys(options.scale)) {
			if (options.scale[key]) {
				toWeight.push(key);
			}
		}
		let results = [];

		results.push(this.simulate("dps", this.iterations, callback));
		for (let weight of toWeight) {
			let initialStat = this.stats[weight];
			let damages = [];
			let weightLoops = 10;
			let weightIncrease = 100;
			for (let i = 0; i < weightLoops; i++) {
				this.stats[weight] += weightIncrease;
				this.stats.calculateModifiers();
				damages.push(this.simulate(weight, this.iterations / 10, callback));
			}
			let increase = 0;
			for (let i = 0; i < 9; i++) {
				increase += damages[i + 1].totalDamage - damages[i].totalDamage;
			}
			increase = increase / (this.iterations / weightLoops) * (1000 / this.duration) / weightIncrease;
			this.stats[weight] = initialStat;
			results.push({
				roll: weight,
				averageDPSIncrease: increase
			});
		}

		callback({
			type: "result",
			result: results
		});
		console.log("COMPLETED IN " + this.round((Date.now() - before) / 1000, 2) + " s");

	}

	/**
	 * @param {string} roll
	 * @param {number} iterations
	 * @param {Function} callback
	 */
	simulate(roll, iterations, callback) {

		let totalDamage = 0;
		let gcdTimer = 0;
		let offGcdTimer = 0;
		let aaTimer = 0;
		let debuffTimer = 0;
		let actions = [];
		let prioList = /** @type {PriorityList} */ (this.prioLists.filter(p => p.class == this.className)[0]);

		for (let i = 0; i < iterations; i++) {

			// Reset buffs, cooldowns, etc.
			gcdTimer = 0;
			offGcdTimer = 0;
			aaTimer = 0;
			debuffTimer = 0;
			this.reset();

			// TODO - Prepull

			while (this.currentTime < this.duration) {

				// GCD attacks
				if (gcdTimer <= 0) {
					let spellToCast = prioList.getNextSpell(this);
					let damage = this.computeSpellDamage(spellToCast);
					spellToCast.onCast(this, damage);
					totalDamage += damage.damage;
					if (roll == "dps" && i == 0) {
						actions.push({
							type: "gcd",
							spell: spellToCast,
							time: this.currentTime,
							damage: damage,
							buffs: this.buffs.slice(),
							debuffs: this.debuffs.slice(),
							resources: Array.from(this.resources.values()).filter(r => r.amount > 0).map(r => {
								return {
									id: r.id,
									name: r.name,
									amount: r.amount
								};
							})
						});
					}
					gcdTimer += spellToCast.gcd || this.stats.getBaseGcd(this);
					if (spellToCast.cast > 0) {
						gcdTimer = Math.max(gcdTimer, spellToCast.cast);
						offGcdTimer = Math.max(offGcdTimer, spellToCast.cast);
						aaTimer = Math.max(aaTimer, spellToCast.cast);
					} else {
						offGcdTimer = Math.max(offGcdTimer, 0.75 * 1000);
					}
					this.lastAction = spellToCast.id;
					if (spellToCast.breaksCombos) {
						this.lastComboAction = spellToCast.id;
					}
				}

				// Off-GCD attacks
				if (offGcdTimer <= 0) {
					let spellToCast = prioList.getNextInstantSpell(this);
					if (spellToCast && (gcdTimer >= 0.75 * 1000 || spellToCast.canClip(this))) {
						let damage = this.computeSpellDamage(spellToCast);
						spellToCast.onCast(this, damage);
						totalDamage += damage.damage;
						if (roll == "dps" && i == 0) {
							actions.push({
								type: "offgcd",
								spell: spellToCast,
								time: this.currentTime,
								damage: damage,
								buffs: this.buffs.slice(),
								debuffs: this.debuffs.slice(),
								resources: Array.from(this.resources.values()).filter(r => r.amount > 0).map(r => {
									return {
										id: r.id,
										name: r.name,
										amount: r.amount
									};
								})
							});
						}
						gcdTimer = Math.max(gcdTimer, 750);
					}
					offGcdTimer += 0.75 * 1000;
				}

				// Auto attacks
				if (aaTimer <= 0) {
					let damage = this.computeAutoAttackDamage();
					totalDamage += damage.damage;
					aaTimer += this.stats.weaponDelay * 1000;
					if (roll == "dps" && i == 0) {
						actions.push({
							type: "autoattack",
							spell: {id: "auto_attack", name: "Auto Attack"},
							time: this.currentTime,
							damage: damage,
							buffs: this.buffs.slice(),
							debuffs: this.debuffs.slice(),
							resources: Array.from(this.resources.values()).filter(r => r.amount > 0).map(r => {
								return {
									id: r.id,
									name: r.name,
									amount: r.amount
								};
							})
						});
					}
				}

				// Debuffs
				if (debuffTimer <= 0) {
					for (let debuff of this.debuffs) {
						let damage = debuff.tick(this);
						if (damage) {
							totalDamage += damage.damage;
							if (roll == "dps" && i == 0) {
								actions.push({
									type: "dot",
									spell: debuff,
									time: this.currentTime,
									damage: damage,
									buffs: debuff.snapshotBuffs,
									debuffs: []
								});
							}
						}
					}
					debuffTimer += 3 * 1000;
				}

				// Manage timing
				let timeToSpend = Math.min(gcdTimer, offGcdTimer, aaTimer, debuffTimer);
				gcdTimer -= timeToSpend;
				offGcdTimer -= timeToSpend;
				aaTimer -= timeToSpend;
				debuffTimer -= timeToSpend;
				this.currentTime += timeToSpend;

				// Manage buffs
				for (let i = this.buffs.length - 1; i >= 0; i--) {
					if (this.buffs[i].remaining > timeToSpend) {
						this.buffs[i].remaining -= timeToSpend;
					} else {
						this.buffs[i].onExpire(this);
						this.buffs.splice(i, 1);
					}
				}

				// Manage debuffs
				for (let i = this.debuffs.length - 1; i >= 0; i--) {
					if (this.debuffs[i].remaining > timeToSpend) {
						this.debuffs[i].remaining -= timeToSpend;
					} else {
						this.debuffs.splice(i, 1);
					}
				}

				// Manage spell cooldowns
				for (let spell of this.spellsArray) {
					if (spell.cooldown > 0) {
						spell.cooldown -= timeToSpend;
						if (spell.cooldown < 0) {
							spell.cooldown = 0;
						}
					}
				}

			}

			callback({ type: "iteration" });

		}

		let data = {
			roll: roll,
			totalDamage: totalDamage,
			averageDamage: totalDamage / iterations,
			averageDPS: totalDamage / iterations * 1000 / this.duration,
			actions: actions.length > 0 ? actions : null,
			averageDPSIncrease: 0
		};
		return data;

	}

	/**
	 * @param {Spell} spell
	 */
	computeSpellDamage(spell) {

		// Calculate buff effects
		let buffEffect = new BuffEffect();
		buffEffect.potency = spell.getPotency(this);
		for (let buff of this.buffs) {
			buff.applyEffect(this, spell, buffEffect);
		}
		for (let debuff of this.debuffs) {
			debuff.applyEffect(this, spell, buffEffect);
		}

		return this.computeDamage(buffEffect.potency, buffEffect, false, false);

	}

	computeAutoAttackDamage() {

		// Calculate buff effects
		let buffEffect = new BuffEffect();
		for (let buff of this.buffs) {
			buff.applyEffect(this, null, buffEffect);
		}
		for (let debuff of this.debuffs) {
			debuff.applyEffect(this, null, buffEffect);
		}

		return this.computeDamage(this.className == "brd" || this.className == "dnc" || this.className == "mch" ? 100 : 110, buffEffect, true, false);

	}

	/**
	 * @param {Debuff} debuff
	 * @param {Buff[]} buffs
	 */
	computeDotDamage(debuff, buffs) {

		// Calculate buff effects
		let buffEffect = new BuffEffect();
		for (let buff of buffs) {
			buff.applyEffect(this, debuff, buffEffect);
		}
		for (let debuff of this.debuffs) {
			debuff.applyEffect(this, debuff, buffEffect);
		}

		return this.computeDamage(debuff.potency, buffEffect, false, true);

	}

	/**
	 * @param {number} potency
	 * @param {BuffEffect} buffEffect
	 * @param {boolean} autoAttack
	 */
	computeDamage(potency, buffEffect, autoAttack = false, dot = false) {

		let hasCrit = false;
		let hasDirect = false;
		let ptc = potency / 100;
		if (!autoAttack) {
			ptc *= this.damagePassives[this.className];
		}

		let wd = autoAttack ? this.stats.autoAttackMultiplier : this.stats.weaponDamageMultiplier;
		let dotSks = 1;
		if (dot) {
			dotSks = this.stats.dotSkillSpeedMultiplier;
		}

		// Critical Strike
		let chp = Math.floor(200 * (this.stats.criticalHit - this.stats.lvlSub) / this.stats.lvlDiv + 50) / 1000 + buffEffect.critChanceFlat;
		let chr = 1;
		if (Math.random() <= chp) {
			chr = Math.floor(200 * (this.stats.criticalHit - this.stats.lvlSub) / this.stats.lvlDiv + 1400) / 1000;
			hasCrit = true;
		}

		// Direct Hit
		let dhp = Math.floor(550 * (this.stats.directHit - this.stats.lvlSub) / this.stats.lvlDiv) / 1000 + buffEffect.dhChanceFlat;
		let dhr = 1;
		if (Math.random() <= dhp) {
			dhr = 1.25;
			hasDirect = true;
		}

		let totalDamage = Math.floor(ptc * wd * this.stats.attackPowerMultiplier * this.stats.determinationMultiplier * dotSks * chr * dhr);
		let variation = (Math.random() - 0.5) / 10;
		totalDamage = Math.floor(totalDamage * (1 + variation))
		totalDamage = Math.floor(totalDamage * buffEffect.damageMultiplier);
		return {
			damage: totalDamage,
			criticalHit: hasCrit,
			directHit: hasDirect
		};

	}

	round(number, decimals = 0) {
		return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
	}

}