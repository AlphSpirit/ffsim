# ffsim

A rotation simulator for Final Fantasy&reg; XIV: A Realm Reborn&trade;.

## Features implemented
- Average DPS simulation
- Primary and secondary stats weighting
- Output of a sample rotation
- Character import

## Classes implemented
- BRD (Bard)
- DNC (Dancer) (WIP)
- SAM (Samurai)

## Can I help?
Sure! Any help is greatly appreciated. If you want to help find bugs, you can always create an Issue with the bug, the steps to recreate it and preferably a screenshot of it.

If you want to help on the code side of things, you can fork the project, code your feature or fix, and then ask for a merge request. We will review, give feedback and hopefully merge your code into the main branch!

## How to setup
- Clone the project
- Run `npm i`
- Start a command line and execute `rollup -w -c`
- Start a second command line and execute `npm run serve`
- Open the project at `localhost:8080`

## How to build
- Exectue `rollup -c`
- Execute `npm run build`
- The compilted website is now in the `dist` folder